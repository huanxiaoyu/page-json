function installAll(_this) {
    layui.use(['jquery', 'form', 'element', 'colorpicker', 'upload'], function () {
        
        var $ = layui.$;

        
        $.each(_this.modules, function (index, item) {
            // 富文本
            console.log("富文本");
            if (item.type == 1) {
                installWangEditor({
                    elem: '#mzifwb' + item.tabName,
                    getText: function (newHtml) {
                        item.children.text = newHtml;
                    }
                });
            }
            if (item.type >= 3) {
                installWangEditor({
                    elem: '#sp-zifxk' + item.tabName,
                    getText: function (newHtml) {
                        item.characters.text = newHtml;
                    }
                });
            }

        });

    });
}

function installOne(params) {
    console.log(params.module.tabName);
    if (params.type >= 3) {
            
        // 初始化视频文字中的文字富文本
        installWangEditor({
            elem: '#sp-zifxk' + params.module.tabName,
            getText: function (newHtml) {
                params.module.characters.text = newHtml;
            }
        });


    }
    if (params.type == 1) {
        // 初始化文字模板中的富文本
        installWangEditor({
            elem: '#mzifwb' + params.module.tabName,
            getText: function (newHtml) {
                params.module.children.text = newHtml;
            }
        });
    }
}
/**
 * 初始化富文本
 * @param {*} params.elem div的id ---带#号
 * @param {*} params.getText 返回的html数据
 */
function installWangEditor(params) {
    const E = window.wangEditor;
    const editor = new E(params.elem);
    // 配置 onchange 回调函数
    editor.config.onchange = function (newHtml) {
        params.getText(newHtml);
    }
    // 配置触发 onchange 的时间频率，默认为 200ms
    editor.config.onchangeTimeout = 500; // 修改为 500ms
    editor.config.menus = [
        'head',
        'bold',
        'fontSize',
        'fontName',
        'italic',
        'underline',
        'strikeThrough',
        'lineHeight',
        'foreColor',
        'link',
        'justify',
    ]
    editor.create();
}